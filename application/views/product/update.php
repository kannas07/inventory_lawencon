<!DOCTYPE html>
<html>
<head>
    <title><?= $title; ?></title>
    <style>
    input[type=text], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }

    input[type=number], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }

    input[type=date], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }

    input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    }

    input[type=submit]:hover {
    background-color: #45a049;
    }

    div {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
    }
    </style>
</head>
<body>
    <?php if ($this->session->flashdata('success')): ?>
    <div>
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div>
        <a href="<?php echo site_url('inventory/') ?>"> Back</a>
    </div>
    
    <div>
        <form method="post" action="<?php echo site_url()."/inventory/update"; ?>">
            <label for="NamaBarang">Product Name *</label>
            <input type="hidden" id="IdBarang" name="IdBarang" value="<?= $data->id; ?>" >
            <input type="text" id="vNamaBarang" name="vNamaBarang" placeholder="Product name.." value="<?= $data->vNamaBarang; ?>" required>

            <label for="KodeBarang">Product Code *</label>
            <input type="text" id="vKodeBarang" name="vKodeBarang" placeholder="Product code .." value="<?= $data->vKodeBarang; ?>" required>

            <label for="country">Product Qty *</label>
            <input type="number" id="iJumlahBarang" name="iJumlahBarang" placeholder="Product Qty .." value="<?= $data->iJumlahBarang; ?>" required>

            <label for="country">Date *</label>
            <input type="date" id="dTanggal" name="dTanggal" placeholder="Date .." value="<?= $data->dTanggal; ?>" required>
        
            <input type="submit" value="Submit">
        </form>
    </div>
</body>
</html>