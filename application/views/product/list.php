<!DOCTYPE html>
<html>
<head>
<title><?= $title; ?></title>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}
div {
    padding: 5px;
}

.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}

.button-primary {
  background-color: blue; 
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}

input[type=text], select {
    /* width: 100%; */
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }
</style>
</head>
<body>

<div>
    <h2><?= $title; ?></h2>
</div>

<div>
<a href="<?php echo site_url('inventory/form_add') ?>" class="button-primary"> Add New</a>
</div>

<div style="text-align:right;width:100%;">
<form method="post" action="<?php echo site_url()."/inventory"; ?>">
    <label for="search">Product Name / Product Code</label>
    <input type="text" name="search" id="search" placeholder="Search Here">
    <input type="submit" class="button" value="Submit">
</form>
</div>

<table id="customers">
  <tr>
    <th>ID</th>
    <th>Product Name</th>
    <th>Product Code</th>
    <th>Product Qty</th>
    <th>Date</th>
    <th>Action</th>
  </tr>
    <?php if(isset($data)) : ?>
    <?php foreach($data as $rows) : ?>
        <tr>
            <td><?= $rows->id; ?></td>
            <td><?= $rows->vNamaBarang; ?></td>
            <td><?= $rows->vKodeBarang; ?></td>
            <td><?= $rows->iJumlahBarang; ?></td>
            <td><?= $rows->dTanggal; ?></td>
            <td>
                <a href="<?php echo site_url('inventory/form_update/'.$rows->id) ?>"> Update</a>
            </td>
        </tr>
    <?php endforeach; ?>
    <?php else : ?>
        <tr>
            <td colspan="6" style="text-align:center;">No Data</td>
        </tr>
    <?php endif; ?>
</table>
</body>
</html>