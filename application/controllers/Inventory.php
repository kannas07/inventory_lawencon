<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller 
{
    public $dbset;

    function __construct()
    {
        parent::__construct();
        $this->dbset = $this->connect();
        $this->load->database();
    }

	public function index()
	{
        $post = $this->input->post();
        
        if(isset($post['search'])) {
            $search = $post['search'];
            $data['data'] = $this->search($search);
        } else {
            $getProduct = $this->db->get('product');
            if($getProduct->num_rows() > 0) {
                $data['data'] = $getProduct->result();
            }
        }

        $data['title'] = "List Product Inventory";
        $this->load->view('product/list', $data);
	}

    Public function connect(){
        $servername = "localhost";
        $database = "lawencon_annas";
        $username = "root";
        $password = "";
        // Create connection
        $conn = mysqli_connect($servername, $username, $password, $database);
        // Check connection
        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }
        // echo "Connected successfully";
        mysqli_close($conn);
    }

	Public function add(){
        $post = $this->input->post();
        $data = array(
            'vNamaBarang' => $post['vNamaBarang'],
            'vKodeBarang' => $post['vKodeBarang'],
            'iJumlahBarang' => $post['iJumlahBarang'],
            'dTanggal' => $post['dTanggal']
        );

        $add = $this->db->insert('product', $data);
        if($add) {
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect(site_url()."/inventory/form_add");
        }
    }

	Public function update() {
        $post = $this->input->post();
        $id = $post['IdBarang'];
        $data = array(
            'vNamaBarang' => $post['vNamaBarang'],
            'vKodeBarang' => $post['vKodeBarang'],
            'iJumlahBarang' => $post['iJumlahBarang'],
            'dTanggal' => $post['dTanggal']
        );

        $this->db->where('id', $id);
        $update = $this->db->update('product', $data);
        if($update) {
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect(site_url()."/inventory/form_update/".$id);
        }
    }

	Public function search($search) {
        $search = trim($search);
        $this->db->where("(vNamaBarang LIKE '%{$search}%' OR vKodeBarang LIKE '%{$search}%')", null);
        $data = $this->db->get('product')->result();
        return $data;
    }

    function form_add() {
        $data['title'] = "Add Product Inventory";
        $this->load->view('product/add', $data);
    }

    function form_update($id) {
        $getProduct = $this->db->get_where('product', array('id' => $id));
        if($getProduct->num_rows() > 0) {
            $data['data'] = $getProduct->row();
        }

        $data['title'] = "Update Product Inventory";
        $this->load->view('product/update', $data);
    }

}
